package de.uni_luebeck.isp.siso.quickcheck;

import com.pholser.junit.quickcheck.Property;
import com.pholser.junit.quickcheck.generator.InRange;
import com.pholser.junit.quickcheck.runner.JUnitQuickcheck;

import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(JUnitQuickcheck.class)
public class PrimeNumberTests {

    @Property
    public void primeFactorsShouldBePrimeNumbers(@InRange(min = "2", max = "999999999") Long a) {
        PrimeCalculator.primeFactorization(a).forEach(e -> assertTrue(PrimeCalculator.isPrime(e)));
    }


    /**
     * The product of all factors has be the number the factors are factorizing.
     * @param a A random number between 2 and 999999999.
     */
    @Property
    public void primeFactorsShouldMultiplyToTheOriginalNumber(@InRange(min = "2", max = "999999999") Long a) {
        Long product = 1L;
        for (long e : PrimeCalculator.primeFactorization(a)) product *= e;
        assertEquals(a, product);
    }

    /**
     * A prime factor must be less or equal the original number
     * @param a A random number between 2 and 999999999.
     */
    @Property
    public void primeFactorsShouldBeLeqTheOriginalNumber(@InRange(min = "2", max = "999999999") Long a) {
        PrimeCalculator.primeFactorization(a).forEach(e -> assertTrue(e<=a));
    }


}